$(function () {
  $("[data-toggle='tooltip']").tooltip()
  $("[data-toggle='popover']").popover()
  $(".carousel").carousel({
    interval: 5000,
  })
  $("#modalReserva").on("show.bs.modal", function (e) {
    console.log("El modal comienza a abrirse")
    $(".btn-reserva").css({"background-color": "#343a40"})
    $(".btn-reserva").prop("disabled", true)
  })
  $("#modalReserva").on("shown.bs.modal", function (e) {
    console.log("El modal terminó de abrirse")
  })
  $("#modalReserva").on("hide.bs.modal", function (e) {
    console.log("El modal comenzó a cerrarse")
  })
  $("#modalReserva").on("hidden.bs.modal", function (e) {
    console.log("El modal terminó de cerrarse")
    $(".btn-reserva").prop("disabled", false)
    $(".btn-reserva").css({'background-color': "#007bff"})
  })
})
